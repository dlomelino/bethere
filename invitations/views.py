from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.contrib.auth.models import User
from django.conf import settings
from invite.custom import access_denied
from invitations import models
from invitations.forms import EventForm
from invitations.functions import *


@login_required
def new_user(request):
    u = User.objects.get(id=request.user.id)

    if not u.contact:
        # Hook this user up to our extended User model
        # new_contact = models.Contact()
        pass

    return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)


@login_required
def events(request, filter=None):
    user = User.objects.get(username=request.user)
    nowdate = timezone.now().strftime("%Y-%m-%d 00:00:00+00:00")

    filters = [
        'going',
        'pending',
        'declined',
        'past',
        'hosting',
    ]

    kwargs = {
        'contact': user,
    }

    if not filter or filter == 'going':
        kwargs['event__date__gte'] = nowdate
        kwargs['status'] = 'going'
    elif filter == 'pending':
        kwargs['event__date__gte'] = nowdate
        kwargs['status'] = 'pending'
    elif filter == 'declined':
        kwargs['event__date__gte'] = nowdate
        kwargs['status'] = 'declined'
    elif filter == 'past':
        kwargs['event__date__lt'] = nowdate
    elif filter == 'hosting':
        kwargs['event__date__gte'] = nowdate
        kwargs['event__host__id'] = user.id

    invites = models.Invitation.objects.filter(**kwargs)

    context = {
        'contact': user,
        'invitations': invites,
    }

    return render(request, 'invitations/invite.html', context)


@login_required
def detail(request, id):
    # Redirect if the Event doesn't exist.
    # This basically happens when someone changes the URL manually
    try:
        event = models.Event.objects.get(id=id)
    except ObjectDoesNotExist:
        access_denied()

    # Get the Invitation details for the selected
    # Event that you are invited to
    invitations = models.Invitation.objects.filter(
        event__id=id,
    )

    if not invitations:
        # You are not on the invite list for this event
        # OR the event ID does not exist. Nice try.
        return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)

    going = []
    pending = []

    for invitation in invitations:
        if invitation.status == 'going':
            going.append(invitation)
        elif invitation.status == 'pending':
            pending.append(invitation)

    context = {
        'event': event,
        'going': going,
        'pending': pending,
    }

    return render(request, 'invitations/detail.html', context)


@login_required
def create(request):
    context = {}

    if request.method == 'POST':
        # A POST request; the submitted form
        form = EventForm(request.POST)
        if form.is_valid():
            # Save the newly created Event with Host set to the current user
            event = form.save(commit=False)
            event.host = request.user
            event.save()

            data = qdict_to_dict(form.data)

            # Create the Invitations for this event from the list of people invited.
            for contact in data['invited']:
                # Load the User object for this contact to attach to the Invitation
                this_user = models.User.objects.get(id=contact)
                invitation = models.Invitation(
                    event=event,
                    contact=this_user,
                    status='pending',
                )

                invitation.save()

            return HttpResponseRedirect(reverse('invitations:detail', kwargs={'id': event.pk}))
    else:
        # Only get users that have signed in through the app (contact is not null).
        # This will exclude things like the django admin and other auxiliary users
        # that should not be in the list.
        users = User.objects.filter(is_active=True, contact__isnull=False)
        context['users'] = users

        form = EventForm()

    context['form'] = form

    return render(request, 'invitations/create.html', context)
