from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


class Contact(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # Add custom fields here to attach to django User object

    def __unicode__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)


class Event(models.Model):
    title = models.CharField(max_length=100, blank=False)
    host = models.ForeignKey(User)
    description = models.TextField(blank=True)
    date = models.DateTimeField(blank=False)
    location = models.CharField(max_length=100, blank=False)
    min_guests = models.IntegerField(blank=True, default=0)
    max_guests = models.IntegerField(blank=True, default=0)
    deadline = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return self.title


class Invitation(models.Model):
    choices = (
        ('pending', 'Pending'),
        ('going', 'Going'),
        ('declined', 'Declined'),
    )
    event = models.ForeignKey(Event)
    contact = models.ForeignKey(User)
    status = models.CharField(max_length=50, choices=choices, default='pending')

    def __unicode__(self):
        return '%s %s is invited to %s' % (self.contact.first_name, self.contact.last_name, self.event.title)
