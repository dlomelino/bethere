from django.conf.urls import url
import views

urlpatterns = [
    url(r'^newuser/$', views.new_user, name="new_user"),
    url(r'^$', views.events, name="events"),
    url(r'^filter/(?P<filter>\w+)', views.events, name="events"),
    url(r'^details/(?P<id>\d+)', views.detail, name="detail"),
    url(r'^create/$', views.create, name="create"),
]
