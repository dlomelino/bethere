from django import forms
from invitations.models import Event


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['title', 'description', 'location', 'min_guests', 'date', 'deadline']
