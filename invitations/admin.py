from django.contrib import admin
from invitations.models import *


class ContactAdmin(admin.ModelAdmin):
    list_display = ['']


admin.site.register(Contact)
admin.site.register(Event)
admin.site.register(Invitation)
