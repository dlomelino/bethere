# Convert a QueryDict into a standard python dict
def qdict_to_dict(qdict):
    return {k: v[0] if len(v) == 1 else v for k, v in qdict.lists()}
