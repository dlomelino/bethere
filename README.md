# invite

This is a python django app. The steps to run it are as follows:

### First-time setup
This assumes Mac OS X and that you already have python 2.x installed and configured for your OS environment.
- Use easy_install to install pip, the python package manager that you will use to install any python modules you need
```
sudo easy_install pip
```
- Install virtualenv
```
sudo pip install virtualenv
```

- Go to the directory that you would like to clone this repository into
```
cd /Users/<username>/code
```
- Clone this repository
```
git clone <repository_uri>
```
- Go into the local repository directory
```
cd invite
```
- Create a virtual environment to work out of
```
virtualenv invite-env
```
- Activate the virtual environment
```
source ./invite-env/bin/activate
```
- Install the required python modules from the pip requirements file
```
pip install -r requirements.txt
```
If you get permission errors, use sudo:
```
sudo pip install -r requirements.txt
```

The Facebook app for Facebook authentication requires you to access the invite app using the specifically configured
URL of 'http://invite.com:9999'. In this case, an additional configuration step is needed:
- Set invite.com in your hosts file to point to your local machine
```
sudo echo '127.0.0.1 invite.com' >> /etc/hosts
```

**Note #1**: The Facebook app is in development mode, which means that you have to be approved as a contributing developer by an
admin in order to log in using Facebook. Again, chances are that if you're working on this project someone can hook
you up.

**Note #2**: You will be missing the settings.py file, since it is bad practice to store it in the repository. Chances are,
if you're working on this project you know someone that can send it to you. Otherwise, you're on your own, so I hope
you are familiar enough with django to create your own! :) (You will be missing the Facebook app keys, however)

### Already set up
This assumes that you have your virtual environment set up and the django app is in a working state.
- Go to the django directory containing the manage.py script (where you cloned the code into)
```
cd /Users/<username>/code/invite
```
- Activate the virtual environment
```
source ./invite-env/bin/activate
```
- Fire up the django server on localhost:9999 specifically (required for Facebook authentication)
```
./manage.py runserver invite.com:9999
```

Now you can browse to http://invite.com:9999 and use the app!
